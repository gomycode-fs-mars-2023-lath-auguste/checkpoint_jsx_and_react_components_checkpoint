import React from 'react';
import FullName from './Component/Profile/FullName';
import Address from './Component/Profile/Address';
import ProfilePhoto from './Component/Profile/ProfilePhoto';
import 'bootstrap/dist/css/bootstrap.min.css';
import './carte.css'

const App = () => {
  return (
    <>
    <div className="container pt-5 mt-5">
      <div className="row d-flex justify-content-center align-items-center">
        <div className="col-6">
            <div className='d-flex py-3 px-3 maCarte'>
              <div className='image'><ProfilePhoto/></div>
              <div className='px-3'>
                <p><span className='fw-bold fs-4'>Full Name</span>  <FullName/></p>
                <p><span className='fw-bold fs-4'>Address</span>  <Address/></p>
              </div>
            </div>
        </div>
      </div>
    </div>
      
    </>
  );
};

export default App;