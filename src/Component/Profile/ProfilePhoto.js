import React from 'react';

const ProfilePhoto = () => {
    return (
        <div>
            <img src="https://picsum.photos/seed/picsum/200" alt="" />
        </div>
    );
};

export default ProfilePhoto;